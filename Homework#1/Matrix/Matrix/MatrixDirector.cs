﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    public class MatrixDirector
    {
        private Matrix _matrix;

        private MatrixDirector()
        {
            _matrix = new Matrix();
            _matrix.FillWithRandomValues();
        }

        private static MatrixDirector _instance;
        private static readonly object SyncRoot = new object();
        public static MatrixDirector Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new MatrixDirector();
                    }
                }
                return _instance;
            }
        }

        public void CreateMatrix(int rows, int cols)
        {
            _matrix = new Matrix(rows, cols);
            _matrix.FillWithRandomValues();
        }

        public void DisplayMatrix()
        {
            Console.WriteLine("Matrix:");
            _matrix.Display();
            Console.WriteLine();
        }

        public void DisplayFirstNotNegativeColumnTask()
        {
            Console.Write("Display the number of the first column, which has no negative element: ");
            int index = _matrix.GetFirstIndexOfColumnWithoutNegativeElements();
            if (index != -1)
            {
                Console.Write(index);
            }
            else
            {
                Console.Write("All columns have negative elements");
            }
            Console.WriteLine("\n");
        }

        public void DisplaySortingTask()
        {
            Console.WriteLine("Display matrix, which rows are sorted by the quantity of same elements");
            _matrix.SortRowsBySameElementsQuantity();
            DisplayMatrix();
        }

        public void DisplaySumOfNegativeRowsTask()
        {
            Console.WriteLine($"Display sum of elements in rows, which have at least one negative element: {_matrix.GetSumOfRowsWithNegativeElements()}");
            Console.WriteLine();
        }

        public void DisplaySaddlePointsTask()
        {
            Console.WriteLine("Display rows and columns of all saddle points in matrix:");
            var saddlePoints = _matrix.GetSaddlePoints();
            if (saddlePoints.Count > 0)
            {
                foreach (var point in saddlePoints)
                {
                    Console.WriteLine($"[{point.IndexRow}; {point.IndexColumn}]");
                }
            }
            else
            {
                Console.WriteLine("There is no saddle points in this matrix");
            }
            Console.WriteLine();
        }

        public void DisplayEqualRowsColumnsTask()
        {
            _matrix = new Matrix();
            _matrix.FillWithRandomValues();
            Console.WriteLine("Display indexes of equal rows and columns: ");
            List<int> indexes = _matrix.GetIndexRowsEqualColumns();
            if (indexes.Count > 0)
            {
                foreach (var index in indexes)
                {
                    Console.WriteLine(index);
                }
            }
            else
            {
                Console.WriteLine("There is no such elements");
            }
            Console.WriteLine();
        }

        public void DisplayAllTasks()
        { 
            DisplayFirstNotNegativeColumnTask();
            DisplaySortingTask();
            DisplaySumOfNegativeRowsTask();
            DisplaySaddlePointsTask();
            DisplayEqualRowsColumnsTask();
        }
    }
}
