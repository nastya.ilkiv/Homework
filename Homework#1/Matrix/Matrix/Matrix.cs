﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    public class Matrix
    {
        private readonly int[,] _matrix;

        public int this[int row, int col]
        {
            get { return _matrix[row, col]; }
            set { _matrix[row, col] = value; }
        }

        public Matrix(int rows = 8, int cols = 8)
        {
            _matrix = new int[rows, cols];
        }

        public Matrix(int[,] matrix) : this(matrix.GetLength(0), matrix.GetLength(1))
        {
            Array.Copy(matrix, _matrix, matrix.Length);
        }


        #region Task 2.1
        public int GetFirstIndexOfColumnWithoutNegativeElements()
        {
            for (int col = 0; col < _matrix.GetLength(1); ++col)
            {
                bool noNegative = true;
                for (int row = 0; row < _matrix.GetLength(0); ++row)
                {
                    if (_matrix[row, col] < 0)
                    {
                        noNegative = false;
                        break;
                    }
                }
                if (noNegative)
                {
                    return col;
                }
            }
            return -1;
        }
        #endregion

        #region Task 2.2
        public void SortRowsBySameElementsQuantity()
        {
            int[] maxQuantity = new int[_matrix.GetLength(0)];
            for (int row = 0; row < _matrix.GetLength(0); ++row)
            {
                //get max quantity of same elements in row
                Dictionary<int, int> coincidence = new Dictionary<int, int>();
                for (int col = 0; col < _matrix.GetLength(1); ++col)
                {
                    if (coincidence.ContainsKey(_matrix[row, col]))
                    {
                        ++coincidence[_matrix[row, col]];
                    }
                    else
                    {
                        coincidence[_matrix[row, col]] = 1;
                    }
                }
                maxQuantity[row] = coincidence.Values.Max();
            }

            //selection sort
            for (int i = 0; i < _matrix.GetLength(0) - 1; ++i)
            {
                int min = i;
                for (int j = i + 1; j < _matrix.GetLength(0); ++j)
                {
                    if (maxQuantity[j] < maxQuantity[min])
                    {
                        min = j;
                    }
                }
                Swap(ref maxQuantity[i], ref maxQuantity[min]);
                SwapRows(i, min);
            }
        }

        private void Swap(ref int value1, ref int value2)
        {
            int temp = value1;
            value1 = value2;
            value2 = temp;
        }

        private void SwapRows(int i, int j)
        {
            int columns = _matrix.GetLength(1);
            for (int k = 0; k < columns; ++k)
            {
                Swap(ref _matrix[i, k], ref _matrix[j, k]);
            }
        }
        #endregion

        #region Task 3.1 and 4.2
        public int GetSumOfRowsWithNegativeElements()
        {
            int sum = 0;
            for (int row = 0; row < _matrix.GetLength(0); ++row)
            {
                int rowSum = 0;
                for (int col = 0; col < _matrix.GetLength(1); ++col)
                {
                    //if row has negative element
                    if (_matrix[row, col] < 0)
                    {
                        //get sum of elements of the row
                        for (int k = 0; k < _matrix.GetLength(1); ++k)
                        {
                            rowSum += _matrix[row, k];
                        }
                        break;
                    }
                }
                sum += rowSum;
            }
            return sum;
        }
        #endregion

        #region Task 3.2
        public class SaddlePoint
        {
            public int IndexRow { get; set; }
            public int IndexColumn { get; set; }

            public SaddlePoint(int row, int column)
            {
                IndexRow = row;
                IndexColumn = column;
            }
        }

        public List<SaddlePoint> GetSaddlePoints() 
        {
            List<SaddlePoint> saddlePoints = new List<SaddlePoint>();
	        for (int row = 0; row < _matrix.GetLength(0); ++row)
            {
		        //get index of minimum element in row
		        int indexColumn = GetIndexOfMinElementInRow(row);
                SaddlePoint p = new SaddlePoint(row, indexColumn);
		        //check if it is a saddle point, if true - add it to list
		        if (IsSaddlePoint(p))
                {
			        saddlePoints.Add(p);
		        }
                //check if there is no such value yet (min for row), while such values are present, check if it is a saddle point
                indexColumn = GetIndexOfNextSameNumberInRow(row, indexColumn);
		        while (indexColumn != -1)
                {
			        SaddlePoint point = new SaddlePoint(row, indexColumn);
			        if (IsSaddlePoint(point))
                    {
				        saddlePoints.Add(point);
			        }
                    indexColumn = GetIndexOfNextSameNumberInRow(row, indexColumn);
                }
	        }
	        return saddlePoints;
        }

        private bool IsSaddlePoint(SaddlePoint point)
        {
            for (int row = 0; row < _matrix.GetLength(0); ++row)
            {
                if (_matrix[point.IndexRow, point.IndexColumn] >= _matrix[row, point.IndexColumn])
                {
                    continue;
                }
                return false;
            }
            return true;
        }

        private int GetIndexOfMinElementInRow(int indexRow)
        {
            int indexColumn = 0;
            int minValue = _matrix[indexRow, 0];

            for (int i = 1; i < _matrix.GetLength(1); ++i)
            {
                if (_matrix[indexRow, i] < minValue)
                {
                    minValue = _matrix[indexRow, i];
                    indexColumn = i;
                }
            }
            return indexColumn;
        }

        private int GetIndexOfNextSameNumberInRow(int indexRow, int numberIndex)
        {
            int value = _matrix[indexRow, numberIndex];

            for (int i = numberIndex + 1; i < _matrix.GetLength(1); ++i)
            {
                if (_matrix[indexRow, i] == value)
                {
                    return i;
                }
            }
            return -1;
        }
        #endregion

        #region Task 4.1
        public List<int> GetIndexRowsEqualColumns()
        {
            if (_matrix.GetLength(0) != _matrix.GetLength(1))
                throw new Exception("Matrix must be square.");

            List<int> indexes = new List<int>();
            for (int i = 0; i < _matrix.GetLength(0); ++i)
            {
                if (IsRowEqualCol(i))
                {
                    indexes.Add(i);
                }
            }
            return indexes;
        }

        bool IsRowEqualCol(int index)
        {
            for (int i = 0; i < _matrix.GetLength(0); ++i)
            {
                if (_matrix[index, i] != _matrix[i, index])
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        public void Display()
        {
            for (int row = 0; row < _matrix.GetLength(0); ++row)
            {
                for (int col = 0; col < _matrix.GetLength(1); ++col)
                {
                    Console.Write(_matrix[row, col] + " ");
                }
                Console.WriteLine();
            }
        }

        public void FillWithRandomValues()
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            for (int row = 0; row < _matrix.GetLength(0); ++row)
            {
                for (int col = 0; col < _matrix.GetLength(1); ++col)
                {
                    _matrix[row, col] = random.Next(-100, 100);
                }
            }
        }
    }
}
