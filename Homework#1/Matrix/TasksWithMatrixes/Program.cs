﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrix;
using Matrix = Matrix.Matrix;

namespace TasksWithMatrixes
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MatrixDirector.Instance.CreateMatrix(5, 5);
                MatrixDirector.Instance.DisplayMatrix();
                MatrixDirector.Instance.DisplayAllTasks();
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadKey();
            }
        }
    }
}
