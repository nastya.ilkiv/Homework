﻿using System.Collections.Generic;

namespace BookLibrary.Interfaces
{
    public interface IBookLibrary : ICountingBooks
    {
        List<Department> Departments { get; set; }
        string Name { get; set; }
    }
}
