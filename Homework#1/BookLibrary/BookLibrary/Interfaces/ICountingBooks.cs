﻿namespace BookLibrary.Interfaces
{
    public interface ICountingBooks
    {
        int CountBooks();
    }
}
