﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Interfaces;

namespace BookLibrary
{
    public class BookLibrary: IBookLibrary
    {
        private List<Department> _departments;
        private string _name;
        public List<Department> Departments
        {
            get { return _departments ?? (_departments = new List<Department>()); }
            set { _departments = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public BookLibrary(): this("NULP Library")
        {
        }

        public BookLibrary(string name)
        {
            _name = name;
        }

        public BookLibrary(List<Department> departments, string name)
        {
            _departments = departments;
            _name = name;
        }

        public int CountBooks()
        {
            return Departments.Sum(department => department.CountBooks());
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
