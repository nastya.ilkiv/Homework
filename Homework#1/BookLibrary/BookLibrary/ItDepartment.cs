﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Interfaces;

namespace BookLibrary
{
    public class ItDepartment : Department
    {
        public ItDepartment()
        {
            SkillsToDevelop = "technical";
            SpecialFeature = "computers";
        }
        public ItDepartment(List<IBook> books) : base(books)
        {
            SkillsToDevelop = "technical";
            SpecialFeature = "computers";
        }
        public override string ToString()
        {
            return "IT department";
        }
    }

}
