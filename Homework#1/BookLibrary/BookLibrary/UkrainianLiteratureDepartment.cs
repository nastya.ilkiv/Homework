﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Interfaces;

namespace BookLibrary
{
    public class UkrainianLiteratureDepartment : Department
    {
        public UkrainianLiteratureDepartment()
        {
            SkillsToDevelop = "linguistic";
            SpecialFeature = "linguistic classes";
        }
        public UkrainianLiteratureDepartment(List<IBook> books) : base(books)
        {
            SkillsToDevelop = "linguistic";
            SpecialFeature = "linguistic classes";
        }
        public override string ToString()
        {
            return "Ukrainian literature department";
        }
    }
}
