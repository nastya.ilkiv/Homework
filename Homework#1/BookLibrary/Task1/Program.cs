﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                BookLibraryDirector.Instance.DisplayLibraryDescription();
                Console.WriteLine($"Author who published the most quantity of books - {BookLibraryDirector.Instance.GetObjectWithMaxBookCount(BookLibraryDirector.Instance.Authors)}");
                Console.WriteLine($"Department who owns the most quantity of books - {BookLibraryDirector.Instance.GetObjectWithMaxBookCount(BookLibraryDirector.Instance.BookLibrary.Departments)}");
                Console.WriteLine($"Book who has the least quantity of pages in the library - {BookLibraryDirector.Instance.GetBookWithMinPages()}");
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadKey();
            }
        }
    }
}
