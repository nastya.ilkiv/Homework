﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Model;
using BookLibrary.Model.Interfaces;

namespace BookLibrary
{
    public class BookLibraryDirector
    {
        private IBookLibrary _bookLibrary;
        private List<IAuthor> _authors;

        public IBookLibrary BookLibrary
        {
            get { return _bookLibrary ?? (_bookLibrary = new BookLibrary()); }
            set { _bookLibrary = value; }
        }

        public List<IAuthor> Authors
        {
            get { return _authors ?? (_authors = new List<IAuthor>()); }
            set { _authors = value; }
        }

        private BookLibraryDirector()
        {
            FillWithInitialData();
        }

        private static BookLibraryDirector _instance;
        private static readonly object SyncRoot = new object();
        public static BookLibraryDirector Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new BookLibraryDirector();
                    }
                }
                return _instance;
            }
        }

        public string GetObjectWithMaxBookCount<T>(List<T> objects)
        {
            return objects.Max().ToString();
        }

        public string GetBookWithMinPages()
        {
            IBook min = BookLibrary.Departments.First().Books.First();
            foreach (var department in BookLibrary.Departments)
            {
                if (min.CompareTo(department.Books.Min()) > 0)
                    min = department.Books.Min();
            }
            return min.ToString();
        }

        public void DisplayLibraryDescription()
        {
            _bookLibrary.OrderDepartments();
            Console.WriteLine($"Hello! Welcome to {BookLibrary}!\nLet's get acquainted with out departments.\n");
            BookLibrary.Departments.ForEach(x => x.DisplayDescription());
        }

        public string GetAuthorByBookTitle(string bookTitle)
        {
            IAuthor author = Authors.FirstOrDefault(x => x.PublishedBooks.Exists(b => b.Title == bookTitle));
            if (author != null)
            {
                return author.ToString();
            }
            else
            {
                return "There is no author who published this book";
            }
        }

        public void FillWithInitialData()
        {
            Authors = new List<IAuthor>()
            {
                new Author() {Name = "Steve", Surname = "McConnell"},
                new Author() {Name = "Andrew", Surname = "Troelsen"},
                new Author() {Name = "Herbert", Surname = "Schildt"},
                new Author() {Name = "Taras", Surname = "Shevchenko"},
                new Author() {Name = "Ivan", Surname = "Franko"},
                new Author() {Name = "Lesia", Surname = "Ukrainka"}
            };
            BookLibrary.Departments = new List<Department>()
            {
                new ItDepartment()
                {
                    Books = new List<IBook>()
                    {
                        new Book()
                        {
                            Author = Authors[0],
                            Title = "'Code Complete'",
                            PageQuantity = 1200
                        },
                        new Book()
                        {
                            Author = Authors[1],
                            Title = "'C# 6.0 and the .NET 4.6 Framework Seventh Edition'",
                            PageQuantity = 1300
                        },
                        new Book()
                        {
                            Author = Authors[1],
                            Title = "'Pro C# 5.0 and the .NET 4.5 Framework Sixth Edition'",
                            PageQuantity = 1050
                        },
                        new Book()
                        {
                            Author = Authors[2],
                            Title = "'C++: The Complete Reference'",
                            PageQuantity = 950
                        },
                        new Book()
                        {
                            Author = Authors[2],
                            Title = "'Java: The Complete Reference'",
                            PageQuantity = 1050
                        },
                        new Book()
                        {
                            Author = Authors[2],
                            Title = "'C#: The Complete Reference'",
                            PageQuantity = 1200
                        }
                    }
                },
                new UkrainianLiteratureDepartment()
                {
                    Books = new List<IBook>()
                    {
                        new Book()
                        {
                            Author = Authors[3],
                            Title = "'Kobzar'",
                            PageQuantity = 700
                        },
                        new Book()
                        {
                            Author = Authors[4],
                            Title = "'Z vershyn i nyzyn'",
                            PageQuantity = 250
                        },
                        new Book()
                        {
                            Author = Authors[4],
                            Title = "'Zivyale lystya'",
                            PageQuantity = 200
                        },
                        new Book()
                        {
                            Author = Authors[5],
                            Title = "'Na krylah pisen'",
                            PageQuantity = 300
                        },
                        new Book()
                        {
                            Author = Authors[5],
                            Title = "'Lisova pisnya'",
                            PageQuantity = 150
                        }
                    }
                }
            };
        }
    }
}
