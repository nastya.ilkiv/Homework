﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Linq;
using BookLibrary.Model;
using BookLibrary.Model.Interfaces;

namespace BookLibrary.Serialization
{
    public class LibraryXmlAnalyzer
    {
        #region Read XML
        public void ReadXml(string fileName)
        {
            XDocument xDoc = XDocument.Load(fileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement bookLibraryDirector = xDoc.Element("BookLibraryDirector");
                if (bookLibraryDirector != null && bookLibraryDirector.HasElements)
                {
                    ReadXMLAuthor(bookLibraryDirector);
                    ReadXmlLibrary(bookLibraryDirector);
                }
            }
        }

        private void ReadXMLAuthor(XElement root)
        {
            XElement authors = root.Element("Authors");
            if (authors != null && authors.HasElements)
            {
                BookLibraryDirector.Instance.Authors = new List<IAuthor>();
                foreach (var author in authors.Elements("Author"))
                {
                    if (author != null && author.HasElements)
                    {
                        IAuthor libAuthor = new Author()
                        {
                            Name = author.Element("Name").Value,
                            Surname = author.Element("Surname").Value,
                            PublishedBooks = new List<IBook>()
                        };
                        XElement publishedBooks = author.Element("PublishedBooks");
                        if (publishedBooks != null && publishedBooks.HasElements)
                        {
                            List<IBook> publBooks = new List<IBook>();
                            foreach (var book in publishedBooks.Elements("PublishedBook"))
                            {
                                publBooks.Add(new Book()
                                {
                                    Title = book.Element("Title").Value,
                                    Author = libAuthor,
                                    PageQuantity = (int)book.Element("PageQuantity")
                                });
                            }
                            libAuthor.PublishedBooks = publBooks;
                        }
                        BookLibraryDirector.Instance.Authors.Add(libAuthor);
                    }
                }
            }
        }

        private void ReadXmlLibrary(XElement root)
        {
            XElement bookLibrary = root.Element("BookLibrary");
            if (bookLibrary != null && bookLibrary.HasElements)
            {
                BookLibraryDirector.Instance.BookLibrary = new BookLibrary()
                {
                    Name = bookLibrary.Element("Name").Value
                };

                ReadXmlDepartment(bookLibrary);
            }
        }

        private void ReadXmlDepartment(XElement root)
        {
            XElement departments = root.Element("Departments");
            if (departments != null && departments.HasElements)
            {
                BookLibraryDirector.Instance.BookLibrary.Departments = new List<Department>();
                foreach (var department in departments.Elements())
                {
                    Type departmentType = Type.GetType(department.Name.ToString(), false, true);
                    object libDepartment = Activator.CreateInstance(departmentType);
                    PropertyInfo skillsToDevelopProperty = departmentType.GetProperty("SkillsToDevelop");
                    skillsToDevelopProperty.SetValue(libDepartment,
                        department.Element("SkillsToDevelop").Value);
                    PropertyInfo specialFeatureProperty = departmentType.GetProperty("SpecialFeature");
                    specialFeatureProperty.SetValue(libDepartment,
                        department.Element("SpecialFeature").Value);

                    XElement books = department.Element("Books");
                    if (books != null && books.HasElements)
                    {
                        List<IBook> libBooks = new List<IBook>();
                        foreach (var book in books.Elements("Book"))
                        {
                            libBooks.Add(new Book()
                            {
                                Title = book.Element("Title").Value,
                                Author = BookLibraryDirector.Instance.Authors[(int)book.Element("AuthorId")],
                                PageQuantity = (int)book.Element("PageQuantity")
                            });
                        }
                        PropertyInfo bookProperty = departmentType.GetProperty("Books");
                        bookProperty.SetValue(libDepartment, libBooks);
                    }
                    BookLibraryDirector.Instance.BookLibrary.Departments.Add((Department)libDepartment);
                }
            }
        }
        #endregion

        #region Create XML 
        public void CreateXml(string fileName)
        {
            XElement bookLibraryDirector = new XElement("BookLibraryDirector");

            XElement authors = CreateXmlAuthors();
            XElement bookLibrary = CreateXmlLibrary();

            bookLibraryDirector.Add(authors);
            bookLibraryDirector.Add(bookLibrary);

            var xDoc = new XDocument(bookLibraryDirector);
            xDoc.Save(fileName);
        }

        private XElement CreateXmlAuthors()
        {
            XElement authors = new XElement("Authors");
            for (int i = 0; i < BookLibraryDirector.Instance.Authors.Count; i++)
            {
                XElement author =
                    new XElement("Author",
                        new XElement("Id", i),
                        new XElement("Name", BookLibraryDirector.Instance.Authors[i].Name),
                        new XElement("Surname", BookLibraryDirector.Instance.Authors[i].Surname));
                XElement publishedBooks = new XElement("PublishedBooks");
                foreach (var authorBook in BookLibraryDirector.Instance.Authors[i].PublishedBooks)
                {
                    XElement book = new XElement("PublishedBook",
                        new XElement("Title", authorBook.Title),
                        new XElement("PageQuantity", authorBook.PageQuantity));
                    publishedBooks.Add(book);
                }
                author.Add(publishedBooks);
                authors.Add(author);
            }
            return authors;
        }

        private XElement CreateXmlLibrary()
        {
            XElement bookLibrary = new XElement("BookLibrary",
                new XElement("Name", BookLibraryDirector.Instance.BookLibrary.Name));
            XElement departments = CreateXmlDepartments();
            bookLibrary.Add(departments);
            return bookLibrary;
        }

        private XElement CreateXmlDepartments()
        {
            XElement departments = new XElement("Departments");
            foreach (var libDepartment in BookLibraryDirector.Instance.BookLibrary.Departments)
            {
                XElement department = new XElement(libDepartment.GetType().ToString(),
                    new XElement("SkillsToDevelop", libDepartment.SkillsToDevelop),
                    new XElement("SpecialFeature", libDepartment.SpecialFeature));
                XElement books = new XElement("Books");
                foreach (var libBook in libDepartment.Books)
                {
                    XElement book = new XElement("Book",
                        new XElement("Title", libBook.Title),
                        new XElement("AuthorId", BookLibraryDirector.Instance.Authors.IndexOf(libBook.Author)),
                        new XElement("PageQuantity", libBook.PageQuantity));
                    books.Add(book);
                }
                department.Add(books);
                departments.Add(department);
            }
            return departments;
        }
        #endregion

        public void EditDepartment(string fileName, string department, string specialFeature)
        {
            RemoveOrEdit(fileName, department, specialFeature, true);
        }

        public void RemoveDepartment(string fileName, string department)
        {
            RemoveOrEdit(fileName,department,null,false);
        }

        private void RemoveOrEdit(string fileName, string department, string specialFeature, bool flag)
        {
            XDocument xDoc = XDocument.Load(fileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement bookLibraryDirector = xDoc.Element("BookLibraryDirector");
                if (bookLibraryDirector != null && bookLibraryDirector.HasElements)
                {
                    XElement bookLibrary = bookLibraryDirector.Element("BookLibrary");
                    if (bookLibrary != null && bookLibrary.HasElements)
                    {
                        XElement departments = bookLibrary.Element("Departments");
                        if (departments != null && departments.HasElements)
                        {
                            XElement someDepartment = departments.Element(department);
                            if (someDepartment != null)
                            {
                                if (flag)
                                {
                                    if (someDepartment.HasElements)
                                    {
                                        someDepartment.Element("SpecialFeature").Value = specialFeature;
                                    }
                                }
                                else
                                {
                                    someDepartment.Remove();
                                }
                                xDoc.Save(fileName);
                            }
                        }
                    }
                }
            }
        }
        public void DisplayLibrary(string fileName)
        {
            XDocument xDoc = XDocument.Load(fileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                Console.WriteLine(xDoc.ToString());
            }
        }
    }
}
