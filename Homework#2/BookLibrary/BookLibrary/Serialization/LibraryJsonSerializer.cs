﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Model.Interfaces;
using Newtonsoft.Json;

namespace BookLibrary.Serialization
{
    public class LibraryJsonSerializer
    {
        private IBookLibrary _bookLibrary;
        private readonly JsonSerializer _jsonSerializer;

        public LibraryJsonSerializer(IBookLibrary bookLibrary)
        {
            _bookLibrary = bookLibrary;
            _jsonSerializer = new JsonSerializer()
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                TypeNameHandling = TypeNameHandling.All
            };
        }

        public void SerializeData(string fileName)
        {
            using (FileStream fs = File.Open(fileName, FileMode.OpenOrCreate))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    using (JsonTextWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;
                        jw.IndentChar = ' ';
                        jw.Indentation = 4;

                        _jsonSerializer.Serialize(jw, _bookLibrary);
                    }
                }
            }
        }

        public IBookLibrary DeserializeData(string fileName)
        {
            using (StreamReader streamReader = File.OpenText(fileName))
            {
                string jsonString = streamReader.ReadToEnd();
                var settings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                };
                _bookLibrary = JsonConvert.DeserializeObject<IBookLibrary>(jsonString, settings);
            }
            return _bookLibrary;
        }
    }
}
