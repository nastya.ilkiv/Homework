﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using BookLibrary.Model;
using BookLibrary.Model.Interfaces;

namespace BookLibrary
{
    [DataContract]
    [KnownType(typeof(ItDepartment))]
    [KnownType(typeof(UkrainianLiteratureDepartment))]
    [KnownType(typeof(Book))]
    [KnownType(typeof(Author))]
    [KnownType(typeof(BookLibrary))]
    public class BookLibrary: IBookLibrary
    {
        private List<Department> _departments;
        private string _name;

        [DataMember]
        public List<Department> Departments
        {
            get { return _departments ?? (_departments = new List<Department>()); }
            set { _departments = value; }
        }

        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public BookLibrary(): this("NULP Library")
        {
        }

        public BookLibrary(string name)
        {
            _name = name;
        }

        public BookLibrary(List<Department> departments, string name)
        {
            _departments = departments;
            _name = name;
        }

        public int CountBooks()
        {
            return Departments.Sum(department => department.CountBooks());
        }

        public void OrderDepartments()
        {
            Departments.OrderBy(x => x.CountBooks()).ThenBy(x => x.ToString());
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
