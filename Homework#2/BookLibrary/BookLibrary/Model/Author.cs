﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using BookLibrary.Model.Interfaces;

namespace BookLibrary.Model
{
    [DataContract(IsReference = true)]
    public class Author: IAuthor
    {
        private string _name;
        private string _surname;
        private List<IBook> _publishedBooks;

        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember]
        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        [DataMember]
        public List<IBook> PublishedBooks
        {
            get { return _publishedBooks ?? (_publishedBooks = new List<IBook>()); }
            set
            {
                _publishedBooks = value;
                foreach (var book in _publishedBooks)
                {
                    if (book.Author != this)
                        book.Author = this;
                }
            }
        }

        public Author() : this("Anonimus", "Anonimus")
        {
        }

        public Author(string name, string surname)
        {
            _name = name;
            _surname = surname;
        }

        public Author(string name, string surname, List<IBook> publishedBooks): this(name, surname)
        {
            _publishedBooks = publishedBooks;
            foreach (var book in _publishedBooks)
            {
                if(book.Author != this)
                    book.Author = this;
            }
        }

        public int CountBooks()
        {
            return PublishedBooks.Count;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentException("Cannot compare to null object!");
            }

            IAuthor otherAuthor = obj as IAuthor;
            if (otherAuthor != null)
            {
                return CountBooks().CompareTo(otherAuthor.CountBooks());
            }
            else
            {
                throw new ArgumentException("Object is not an IAuthor!");
            }    
        }

        public override string ToString()
        {
            return $"{_name} {_surname}";
        }

        public void Order()
        {
            PublishedBooks.OrderBy(x => x.Title).ThenBy(x =>x.PageQuantity);
        }
    }
}
