﻿namespace BookLibrary.Model.Interfaces
{
    public interface ICountingBooks
    {
        int CountBooks();
    }
}
