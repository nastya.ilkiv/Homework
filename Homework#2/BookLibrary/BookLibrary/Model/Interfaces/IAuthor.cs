﻿using System;
using System.Collections.Generic;

namespace BookLibrary.Model.Interfaces
{
    public interface IAuthor : ICountingBooks, IComparable
    {
        string Name { get; set; }
        string Surname { get; set; }
        List<IBook> PublishedBooks { get; set; }
    }
}
