﻿using System;

namespace BookLibrary.Model.Interfaces
{
    public interface IBook : IComparable
    {
        string Title { get; set; }
        IAuthor Author { get; set; }
        int PageQuantity { get; set; }
    }
}
