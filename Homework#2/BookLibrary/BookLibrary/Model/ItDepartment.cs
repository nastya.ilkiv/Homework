﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using BookLibrary.Model.Interfaces;

namespace BookLibrary.Model
{
    [DataContract]
    public class ItDepartment : Department
    {
        public ItDepartment()
        {
            SkillsToDevelop = "technical";
            SpecialFeature = "computers";
        }
        public ItDepartment(List<IBook> books) : base(books)
        {
            SkillsToDevelop = "technical";
            SpecialFeature = "computers";
        }
        public override string ToString()
        {
            return "IT department";
        }
    }
}
