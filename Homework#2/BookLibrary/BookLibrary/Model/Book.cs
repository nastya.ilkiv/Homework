﻿using System;
using System.Runtime.Serialization;
using BookLibrary.Model.Interfaces;

namespace BookLibrary.Model
{
    [DataContract(IsReference = true)]
    public class Book: IBook
    {
        private string _title;
        private IAuthor _author;
        private int _pageQuantity;

        [DataMember]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        [DataMember]
        public IAuthor Author
        {
            get { return _author ?? (_author = new Author()); }
            set
            {
                _author = value;
                if (!_author.PublishedBooks.Exists(x => x.ToString() == this.ToString()))
                    _author.PublishedBooks.Add(this);
            }
        }

        [DataMember]
        public int PageQuantity
        {
            get { return _pageQuantity; }
            set { _pageQuantity = value; }
        }

        public Book(): this("Unknown")
        {
        }

        public Book(string title)
        {
            _title = title;
        }

        public Book(IAuthor author): this("Unknown")
        {
            _author = author;

            if (!_author.PublishedBooks.Exists(x => x.ToString() == this.ToString()))
                _author.PublishedBooks.Add(this);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentException("Cannot compare to null object!");
            }

            IBook otherAuthor = obj as IBook;
            if (otherAuthor != null)
            {
                return PageQuantity.CompareTo(otherAuthor.PageQuantity);
            }
            else
            {
                throw new ArgumentException("Object is not an IAuthor!");
            }
        }

        public override string ToString()
        {
            return $"{Title} {Author}";
        }

        public void Order()
        {
            throw new NotImplementedException();
        }
    }
}
