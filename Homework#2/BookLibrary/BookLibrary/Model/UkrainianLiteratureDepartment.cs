﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using BookLibrary.Model.Interfaces;

namespace BookLibrary.Model
{
    [DataContract]
    public class UkrainianLiteratureDepartment : Department
    {
        public UkrainianLiteratureDepartment()
        {
            SkillsToDevelop = "linguistic";
            SpecialFeature = "linguistic classes";
        }
        public UkrainianLiteratureDepartment(List<IBook> books) : base(books)
        {
            SkillsToDevelop = "linguistic";
            SpecialFeature = "linguistic classes";
        }
        public override string ToString()
        {
            return "Ukrainian literature department";
        }
    }
}
