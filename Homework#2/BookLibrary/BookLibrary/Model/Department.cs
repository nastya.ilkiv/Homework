﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using BookLibrary.Model.Interfaces;

namespace BookLibrary.Model
{
    [DataContract]
    [KnownType(typeof(ItDepartment))]
    [KnownType(typeof(UkrainianLiteratureDepartment))]
    public abstract class Department : ICountingBooks, IComparable
    {
        private List<IBook> _books;
        private string _skillsToDevelop;
        private string _specialFeature;

        [DataMember]
        public List<IBook> Books
        {
            get { return _books ?? (_books = new List<IBook>()); }
            set { _books = value; }
        }

        [DataMember]
        public string SkillsToDevelop
        {
            get { return _skillsToDevelop; }
            set { _skillsToDevelop = value; }
        }

        [DataMember]
        public string SpecialFeature
        {
            get { return _specialFeature; }
            set { _specialFeature = value; }
        }

        protected Department(): this("unknown", "unknown")
        {
        }

        protected Department(string skillsToDevelop, string specialFeature)
        {
            _skillsToDevelop = skillsToDevelop;
            _specialFeature = specialFeature;
        }

        protected Department(List<IBook> books): this("unknown", "unknown")
        {
            _books = books;
        }

        public int CountBooks()
        {
            return _books.Count;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Department otherDepartment = obj as Department;
            if (otherDepartment != null)
            {
                return CountBooks().CompareTo(otherDepartment.CountBooks());
            }
            else
            {
                throw new ArgumentException("Object is not a Department");
            }
        }

        public void DisplayDescription()
        {
            Console.WriteLine($"Welcome to the {ToString()} of our library!\n Here you can improve your {SkillsToDevelop} skills by reading " +
                "famous books such as:");
            Books.Sort();
            Books.ForEach(b => Console.Write($"{b} - {b.PageQuantity} pages\n"));
            Console.WriteLine($"Also our department is provided with the {SpecialFeature}, which gives you an opportunity to consolidate your knowledge in practice.\n");
        }
        public abstract override string ToString();
    }
}
