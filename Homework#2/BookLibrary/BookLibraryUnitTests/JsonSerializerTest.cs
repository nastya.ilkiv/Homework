﻿using System;
using BookLibrary;
using BookLibrary.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookLibraryUnitTests
{
    [TestClass]
    public class JsonSerializerTest
    {
        [TestMethod]
        public void TestJsonSerialization()
        {
            LibraryJsonSerializer serializer = new LibraryJsonSerializer(BookLibraryDirector.Instance.BookLibrary);
            serializer.SerializeData("library.json");
        }

        [TestMethod]
        public void TestJsonDeserialization()
        {
            LibraryJsonSerializer serializer = new LibraryJsonSerializer(BookLibraryDirector.Instance.BookLibrary);
            BookLibraryDirector.Instance.BookLibrary = serializer.DeserializeData("library.json");
        }
    }
}
