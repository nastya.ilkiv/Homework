﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using BookLibrary;
using BookLibrary.Serialization;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                LibraryXmlAnalyzer xml = new LibraryXmlAnalyzer();
                xml.ReadXml("library1.xml");
                BookLibraryDirector.Instance.DisplayLibraryDescription();
                Console.WriteLine($"Author who published the most quantity of books - {BookLibraryDirector.Instance.GetObjectWithMaxBookCount(BookLibraryDirector.Instance.Authors)}");
                Console.WriteLine($"Department which owns the most quantity of books - {BookLibraryDirector.Instance.GetObjectWithMaxBookCount(BookLibraryDirector.Instance.BookLibrary.Departments)}");
                Console.WriteLine($"Book which has the least quantity of pages in the library - {BookLibraryDirector.Instance.GetBookWithMinPages()}");
                Console.WriteLine($"Author who published the book 'Code Complete' - {BookLibraryDirector.Instance.GetAuthorByBookTitle("'Code Complete'")}");
                xml.CreateXml("library1.xml");

                xml.EditDepartment("library2.xml", BookLibraryDirector.Instance.BookLibrary.Departments[0].GetType().ToString(), "notebooks");
                xml.RemoveDepartment("library2.xml", BookLibraryDirector.Instance.BookLibrary.Departments[1].GetType().ToString());
                xml.ReadXml("library2.xml");
                BookLibraryDirector.Instance.DisplayLibraryDescription();

                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadKey();
            }           
        }
    }
}
